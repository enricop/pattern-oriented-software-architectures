#!/usr/bin/env python

#Tested with Python 2.7.4, Twisted 13.0.0, GCC 4.8.0 on linux 3.8.8

from twisted.internet import protocol, reactor

# Perform application-specific processing.
# In this case we print the data sent by the client,
# via the connection established by the connector and acceptor components,
# one line at a time
# EchoServerHandler enacpsulates the logig and the methods of 
# streaming connection-oriented protocols.
# (Wrapper Facade)
class EchoServerHandler(protocol.Protocol):
    def dataReceived(self, data):
        for line in data:
            self.transport.write(line)

 # The EchoServerFactory handles the service initialization by
 # initializing a service handler to process data exchanged on the connection.
 # (Acceptor)
class EchoServerFactory(protocol.ServerFactory):
    def buildProtocol(self, addr):
        return EchoServerHandler()

def main():
    #(Reactor)
    reactor.listenTCP(8000, EchoServerFactory())
    #The reactor thread, once started, waits for a connection request on the TCP port.
    #For every TCP session it creates an instance of the EchoServerFactory.

    #In python twisted the reactor object implements the reactor pattern by handling
    #service requests that are delivered concurrently to the application by one or more
    #clients.
    reactor.run()
    
if __name__ == '__main__':
    main()
