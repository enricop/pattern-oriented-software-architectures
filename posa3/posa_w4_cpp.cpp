/* 
 * File:   posa_w4_cpp.txt
 * Author: Enrico Papi
 * Compiler version: gcc 4.8
 * STD lib version: libstdc++6
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
using namespace std;

#define PHILOSOPHERS_NUM 5
#define EATINGTIMES_NUM 5

class Chopstick
{
public:
	Chopstick(){};
	mutex m;
};

int eat(unsigned short mypnum, Chopstick* leftChopstick, Chopstick* rightChopstick) {
	
	unsigned short myeatingtimes = 0;
	do {
		// Implements the Monitor pattern.
		// Ensures there are no deadlocks.
		if (try_lock(leftChopstick->m, rightChopstick->m) != -1)
			continue;

		lock_guard<mutex> a(leftChopstick->m, adopt_lock);
		cout << "Philosopher " + to_string(mypnum) + " picks up left chopstick.\n";

		lock_guard<mutex> b(rightChopstick->m, adopt_lock);					
		cout << "Philosopher " + to_string(mypnum) + " picks up right chopstick.\n";

		cout << "Philosopher " + to_string(mypnum) + " eats.\n";

		chrono::milliseconds timeout(500);
		this_thread::sleep_for(timeout);

		myeatingtimes++;

		cout << "Philosopher " + to_string(mypnum) + " puts down left chopstick.\n";
		cout << "Philosopher " + to_string(mypnum) + " puts down right chopstick.\n";
	} while (myeatingtimes < EATINGTIMES_NUM);
	return 0;
}

int main()
{
	try {
		cout << "Dinner is starting!" << endl << endl;

		// 5 Chopsticks on the left and right of each philosopher.
		// Use them as unique pointers, to acquire locks.
		vector< unique_ptr<Chopstick> > chopsticks(PHILOSOPHERS_NUM);

		for (int i = 0; i < PHILOSOPHERS_NUM; ++i)
		{
			auto c1 = unique_ptr<Chopstick>(new Chopstick());
			chopsticks[i] = move(c1);
		}

		// This is where we create philosophers, each of 5 tasks represents one philosopher.
		vector<thread> tasks(PHILOSOPHERS_NUM);

		tasks[0] = thread(eat, 1,
				chopsticks[0].get(),
				chopsticks[PHILOSOPHERS_NUM - 1].get());

		for (int i = 1; i < PHILOSOPHERS_NUM; ++i)	
		{
			tasks[i] = thread(eat, i+1, 
					chopsticks[i - 1].get(),
					chopsticks[i].get());
		}

		// Start to eat!
		for_each(tasks.begin(), tasks.end(), mem_fn(&thread::join));
		cout << "Dinner is over!" << endl;
	} catch (const exception& e) {
		cerr << "EXCEPTION: " << e.what() << endl;
	}
	return 0;
}