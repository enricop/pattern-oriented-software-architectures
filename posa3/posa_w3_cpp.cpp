/* 
 * File:   posa_w3_cpp.txt
 * Author: Enrico Papi
 *
 * Created on April 8, 2013, 11:22 AM
 */
#include <condition_variable>
#include <mutex>
#include <future>
#include <iostream>
using namespace std;

#define MAX_OUTPUT_PER_THREAD 3

bool t1Turn = true;
mutex printMutex;
condition_variable turnCondVar;
	
void threaded_printer(const string s, const bool turn)
{
	unsigned short printCounter = 0;

	do {
		// locks printMutex if it is not already locked
		unique_lock<mutex> ul(printMutex);

		/*
		 * Waits for a notification from another thread,
		 * using the lock ul,
		 * until the lambda expression returns true.
		 * 
		 * When the lambda expression is true it means it is the turn
		 * of this thread and we will receive a notification soon that
		 * informs the other thread has released the lock.
		 * 
		 * Wait() now automatically locks the mutex for this thread.
		 * 
		 * If we are at the first turn of t1, wait() just returns,
		 * keeping the lock on printMutex.
		 * 
		 * Since there are only two threads we could have omitted the
		 * use of the lambda expression, assuming that if the lock
		 * is not owned by this thread is not the turn of this thread.
		 */
		turnCondVar.wait(ul, [turn]{ return (t1Turn == turn); } );

		//do whatever shall happen during our turn and we have the lock
		cout << s << endl;

		printCounter++;
		t1Turn = !turn;
		
		/*
		 * The mutex must be unlocked before we send the
		 * notification to the other thread.
		 * So we have to explicitly call unlock() instead of waiting the
		 * end of the loop.
		 */
		ul.unlock();
		turnCondVar.notify_one();
	} while(printCounter < MAX_OUTPUT_PER_THREAD);
}

int
main(int argc, char** argv)
{
	try {
		cout << "Ready... Set... Go!" << endl << endl;

		// print 'Ping' in separate thread
		thread t1(threaded_printer, "Ping!", true);
		// print 'Pong' in separate thread
		thread t2(threaded_printer, "Pong!", false);
				
		t1.join();
		t2.join();
		
		cout << "Done!" << endl;
	} catch (const exception& e) {
		cerr << "EXCEPTION: " << e.what() << endl;
	}
	return 0;
}