package posa_w3_java;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
 *
 * @author sunkiss
 */
public class Posa_w3_java {

    private static class ThreadedPrinter implements Runnable {
        
        //private static boolean t1Turn = true;
        
        private static Lock printLock = new ReentrantLock();
        private static Condition turnCond = printLock.newCondition();
   
        private static String mymessage;
        //private static boolean myturn;        
        
        private ThreadedPrinter(String message) {
            mymessage = message;
            //myturn = turn;
        }
        
        @Override
        public void run() {
            try {
                short printCounter = 0;

                do {
                        printLock.lock();
                        turnCond.await();
                        
                        //do whatever shall happen during our turn and we have the lock
                        System.out.println(mymessage);

                        printCounter++;
                        //t1Turn = !myturn;

                        /*
                         * The mutex must be unlocked before we send the
                         * notification to the other thread.
                         * So we have to explicitly call unlock() instead of waiting the
                         * end of the loop.
                         */
                } while(printCounter < 3);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            } finally {
                printLock.unlock();
                turnCond.signal();
            }
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Ready... Set... Go!");
        System.out.println("");
        // Create a thread pool with two threads
        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.execute(new ThreadedPrinter("Ping!"));
        executor.execute(new ThreadedPrinter("Pong!"));
        executor.shutdown();
        System.out.println("Done!");
    }
}
