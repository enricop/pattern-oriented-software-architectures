//-----------------------------------------------------------------------------
// File: Program.h
//
// Desc: Dining Philosophers sample.
//		 
//		 Demonstrates how to use Monitor object (lock) to protect critical section.
//		 
//		 Scenario is such that there are 5 philosophers and 5 tools on each side of a philosopher.
//		 Each philosopher can only take one tool on the left and one on the right.
//		 One of the philosophers must wait for a tool to become available because whoever grabs
//		 a tool will hold it until he eats and puts the tool back on the table.
//		 
//		 Application of the pattern could be transferring money from account A to account B.
//		 Important here is to pass locking objects always in the same (increasing) order.
//		 If the order is mixed you would encounter random deadlocks at runtime.
//
//-----------------------------------------------------------------------------

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
using namespace std;


#define PHILOSOPHERS_NUM 5
#define EATINGTIMES_NUM 5


class Chopstick
{
public:
	Chopstick(){};
	mutex m;
};

int main()
{
	auto eat = [](unsigned short mypnum, Chopstick* leftChopstick, Chopstick* rightChopstick) {
		do {
			if (left == right)
				throw exception("Left and right chopsticks should not be the same!");

			// ensures there are no deadlocks
			if (try_lock(leftChopstick->m, rightChopstick->m) != -1)
				continue;

			lock_guard<mutex> a(leftChopstick->m, adopt_lock);
			string sl = "Philosopher " + to_string(mypnum) + " picks up left chopstick.\n";
			cout << sl.c_str();

			lock_guard<mutex> b(rightChopstick->m, adopt_lock);					
			string sr = "Philosopher " + to_string(mypnum) + " picks up right chopstick.\n";
			cout << sr.c_str();

			string pe = "Philosopher " + to_string(mypnum) + " eats.\n";
			cout << pe;

			chrono::milliseconds timeout(500);
			this_thread::sleep_for(timeout);
			
			myeatingtimes++;
			
			string sr = "Philosopher " + to_string(mypnum) + " puts down left chopstick.\n";
			string sr = "Philosopher " + to_string(mypnum) + " puts down right chopstick.\n";
		} while (myeatingtimes < EATINGTIMES_NUM);
	};

	// 5 Chopsticks on the left and right of each philosopher.
	// Use them to acquire locks.
	vector< unique_ptr<Chopstick> > chopsticks(PHILOSOPHERS_NUM);

	for (int i = 0; i < numPhilosophers; ++i)
	{
		auto c1 = unique_ptr<Chopstick>(new Chopstick());
		chopsticks[i] = move(c1);
	}

	// This is where we create philosophers, each of 5 tasks represents one philosopher.
	vector<thread> tasks(numPhilosophers);

	tasks[0] = thread(eat, i+1,
			chopsticks[0].get(),
			chopsticks[PHILOSOPHERS_NUM - 1].get());

	for (int i = 1; i < numPhilosophers; ++i)	
	{
		tasks[i] = thread(eat, i+1, 
				chopsticks[i - 1].get(),				// left chopstick
				chopsticks[i].get());
	}

	// May eat!
	for_each(tasks.begin(), tasks.end(), mem_fn(&thread::join));

	return 0;
}